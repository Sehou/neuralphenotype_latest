#!/usr/bin/env python3
"""
Author: Kouiho, Sehou Romaric
Registration number: 920726471080

Script to train the Word2Vec algorithm for protein domain embedding.

Required command line inputs to be given:
            The arguments to be provided depends greatly
            on the desired output, For more information about
             how to use it, please type the following command
             in the cmd terminal.
             python parse_and_write_domain_architecture.py --help

WARNING!!! The order of the command line to be given are crucial.

"""

# import statements
import argparse
import logging
import multiprocessing
from pathlib import Path
import gensim.models
import nltk

nltk.download("punkt")
logging.basicConfig(format="%(asctime)s : %(levelname)s : %(message)s",
                    level=logging.INFO)


class MakeCorpus:
    """An interator that yields sentences (lists of str)."""

    def __init__(self, path_to_corpus, delimiter=",", between=". "):
        self.path_to_corpus = path_to_corpus
        self.delimiter = delimiter
        self.between = between

    def __iter__(self):
        logging.info(f"reading file {self.path_to_corpus}...this may "
                     f"take a while")
        with open(self.path_to_corpus, 'r') as f:
            for i, line in enumerate(f):
                if "domain" not in line:

                    if i % 1000 == 0:
                        logging.info("read {0} reviews".format(i))
                    # pre-processing
                    id_sen, sentence = line.lower().split(self.delimiter)
                    sentence = " ".join(sentence.split(self.between))

                    # fit tokenizer on the training set
                    sentence = nltk.word_tokenize(sentence)
                    yield sentence


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Making corpus out of\
     space separate sentences per line, training of word2vec on the \
     corpus and saving model to disk.")

    parser.add_argument("-i", type=str, required=True,
                        help="Enter full path of the corpus file")
    parser.add_argument("-o", type=str, required=True,
                        help="complete path with name of the output model")
    parser.add_argument("-s", type=int, default=100,
                        help="Enter the desired embedding size."
                             "Default = 100")
    parser.add_argument("-n", default=2,
                        help="The desired negative sampling. Default = 2")
    parser.add_argument("-w", default=2,
                        help="The window size to be used. Default is 2")
    parser.add_argument("-m", type=int, default=1,
                        help="The minimum count for the word to be "
                             "included in the vocabulary. Default is 1.")
    parser.add_argument("-job", type=int, default=1,
                        help="The number of worker to used to parallelize"
                             " the computation. Default is 1")
    parser.add_argument("-l", type=bool, default=False,
                        help="Compute loss for the training. "
                             "Default is false.")
    parser.add_argument("-f", type=str, default="binary", choices=["binary", "normal"],
                        help="format of the model to be saved. Default is binary")
    parser.add_argument("-iter", type=int, default=10,
                        help="Number of iteration/epoch to be used"
                             "to train the model. Default is 10.")
    parser.add_argument("-sample", default=3e-5,
                        help="The subsampling. Default is 3e-5")

    args = parser.parse_args()

    input_path = Path(args.i)
    # out_path = input_path.parent/args.o

    # Make corpus iterator object
    sentences = MakeCorpus(input_path)

    # Get the number of cores
    cores = multiprocessing.cpu_count()
    num_job = args.job
    try:
        assert num_job <= cores
    except AssertionError:
        logging.warning(f"The number of cores ({num_job}) is more than the"
                        f" available cores. Using one core for the "
                        f"training")
        num_job = 1

    # get windows, sample, and sampling parameters
    try:
        windows = list(map(int, args.w.split(",")))
        negatives = list(map(int, args.n.split(",")))
        samples = list(map(float, args.sample.split(",")))
    except TypeError and ValueError and IndexError:
        raise IOError("Wrong format for either windows, sample or number "
                      "of samples. Consult help for the good format")

    # build vocabulary and train model
    for window in windows:
        for negative in negatives:
            for sample in samples:
                model = gensim.models.Word2Vec(sentences,
                                               size=args.s,
                                               negative=negative,
                                               sample=sample,
                                               window=window,
                                               min_count=args.m,
                                               workers=num_job,
                                               compute_loss=args.l,
                                               iter=args.iter)

                name_bin = f'{args.o}_{window}_{negative}_{sample}.bin'
                # model.wv.save_word2vec_format(name_bin, binary=True)

                if args.f == "binary":
                    model.wv.save_word2vec_format(f"{name_bin}.bin",
                                                  binary=True)
                else:
                    model.wv.save_word2vec_format(f"{name_bin}.txt",
                                                  binary=False)

                if args.l:
                    logging.info(f"The lastest loss is: "
                                 f"{model.get_latest_training_loss()}")
