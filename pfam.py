#!/usr/bin/env python3
"""
Author: Kouiho, Sehou Romaric
Registration number: 920726471080

Script to retrieve PFAM domain description from Pfam-A.hmm.

Required command line inputs to be given:
            The string of the path to the Pfam-A.hmm is the only
            argument to be provided.
            Type  python pfam.py <path>

WARNING!!! This script does not return anything but it save
the CSV file of the domains description to the current working
directory as "pfam_domain_description.csv".

"""
import logging
from sys import argv
import pandas as pd
logging.basicConfig(format="%(asctime)s : %(levelname)s : %(message)s",
                    level=logging.INFO)


def find_a_hmm_record(file_name):
    """Get a hmm profile for each domain.

    :param file_name: String, path to the Pfam-A.hmm

    :return: Object, a generator object (lazy iterator) of the hmm.profile
            for each pfam domain. This allows to get one genome record
        at a time without reading all the data in memory.
    """

    record = []

    with open(file_name) as pfam_hmm:
        for line in pfam_hmm:
            if not line.strip():
                continue
            line = line.strip()

            if line.startswith("HMMER") and record:
                yield record
                record = []
            else:
                record.append(line)

        if record:
            yield record


def extract_domain_description_from_pfam_hmm(record):
    """Extract the id, accession number and description of a domain.

    :param record: List, list of string line of the hmm profile
                        of a pfam domain.

    :return: List, containing the id, accession number and
                    description of a PFAM domain.
    """

    description = []

    for line in record:
        if line.startswith("NAME"):
            description.append(line.strip("NAME").strip())
        elif line.startswith("ACC"):
            description.append(line.split()[1].split(".")[0])
        elif line.startswith("DESC"):
            description.append(line.strip("DESC").strip())
        else:
            continue

    return description


if __name__ == '__main__':
    # initialize and get input variable
    pfam_dict = {}
    pfam_file = argv[1]

    # get the records as generator
    records = find_a_hmm_record(pfam_file)

    # extract id, accession and description for each domains
    i = 1
    for rec in records:
        logging.info(f"Processing record {i}")
        info = extract_domain_description_from_pfam_hmm(rec)
        pfam_dict[info[1]] = info
        i += 1

    # convert dictionary of data in dataframe
    pfam_df = pd.DataFrame.from_dict(pfam_dict, orient='index',
                                     columns=["id", "accession", "descr"])

    # remove accession as it is be used as index
    pfam_df = pfam_df.drop("accession", axis=1)

    # save dataframe to CSV
    pfam_df.to_csv("pfam_domain_description.csv")
