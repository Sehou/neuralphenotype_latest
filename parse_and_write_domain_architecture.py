#!/usr/bin/env python3
"""
Author: Kouiho, Sehou Romaric
Registration number: 920726471080

Script to parse protein domain based on their position and
gene position in microbial genome. It helps to get the
protein domain per contig, for genes in the same operon,
and in the whole genome.

Required command line inputs to be given:
            The arguments to be provided depends greatly
            on the desired output, For more information about
             how to use it, please type the following command
             in the cmd terminal.
             python parse_and_write_domain_architecture.py --help

WARNING!!! The order of the command line to be given are crucial for
            this script to perform the above-mentioned tasks.

            This script is based on the data streaming process and
            can handle big dataset however it might take about 6hours
            to maximum 2 days to finish running depending on the desired
            output and data size. It might show you some warnings as well.

"""

# import statements
import argparse
import logging
import os
from copy import deepcopy

logging.basicConfig(format="%(asctime)s : %(levelname)s : %(message)s",
                    level=logging.INFO)


# *************** First part: parse genome files to CSV ***************

def get_a_genome_record_at_a_time(data_file, delimiter=","):
    """Get a genome sample records of raw line from raw data file.

    :param data_file: String, name of data file ordered per genome.
    :param delimiter: String, the separator of the entries of the lines
                        Default is a string comma ",".

    :return: Object, a generator object (lazy iterator) of the genome
    sample records in the file. This allows to get one genome record
    at a time without reading all the data in memory.
    """
    genome_record, genome_id = [], ""
    with open(data_file) as data:
        for line in data:
            line = line.strip()
            if not line.startswith("http"):
                continue
            else:
                line_id = line.split(delimiter)[0].split("/")[4]
                if genome_id != "" and genome_id != line_id:
                    yield genome_record
                    genome_record, record_id = [], ""
                genome_id = line_id
                genome_record.append(line)
        if genome_record:
            yield genome_record


def parse_a_protein_record_line(record_line, delimiter=","):
    """Parse a line in a genome record to extract the useful information.

    :param record_line: String, line of a genome record.
    :param delimiter: String, the separator of the entries of the line.
                        Default is a string comma ",".

    :return: List, containing genome ID, gene, start_gene, stop_gene,
                start_domain, stop_domain and domain in the line.
    """
    record_line_data = ""
    record_line = record_line.strip()
    try:
        genome_id, contig, gene, start_gene, start_domain, stop_domain, \
        domain = record_line.split(delimiter)
        contig = (lambda x: x.split("/")[4].split(".")[0])(genome_id)
        gene_coordinates = gene.split("/")
        gene = gene_coordinates[5]
        # start_gene = start_gene.split("/")[-1]
        # stop_gene = gene_coordinates[-1].split("-")[-1]
        try:
            start_gene, stop_gene = gene_coordinates[-1].split("-")
        except ValueError:
            parts = gene_coordinates[-1].split("_")
            for part in parts:
                if start_gene.split("/")[-1] in part:
                    start_gene, stop_gene = part.split("-")
                    break

        start_domain = (lambda x: x.split("/")[-1])(start_domain)
        stop_domain = (lambda x: x.split("/")[-1])(stop_domain)

        # check if the domain is on the forward or reverse strand
        try:
            start_gene, stop_gene = int(start_gene), int(stop_gene)
            start_domain, stop_domain = int(start_domain), int(stop_domain)
            record_line_data = [contig, gene, start_gene, stop_gene,
                                start_domain, stop_domain, domain]

        except ValueError:
            start_gene = int("".join([s for s in start_gene if
                                      s.isdigit()]))
            stop_gene = int("".join([s for s in stop_gene if
                                     s.isdigit()]))
            start_domain = int("".join([s for s in start_domain if
                                        s.isdigit()]))
            stop_domain = int("".join([s for s in stop_domain if
                                       s.isdigit()]))
            record_line_data = [contig, gene, start_gene, stop_gene,
                                start_domain, stop_domain, f"c{domain}"]
    except IndexError:
        length = len(record_line.split(delimiter))
        logging.warning(f"Abnormal length detected for line {record_line}."
                        f"Length 7 was expected but found length {length}")
        pass
    return record_line_data


def write_csv_file_of_domains(parsed_line, out_file, delimiter, run=0):
    """Write out useful information in a CSV file for downstream analysis.

    :param parsed_line: List, containing genome ID, gene, start_gene,
            stop_gene, start_domain, stop_domain and domain in the line.
    :param out_file: String, name of the output CSV file where
                    information must be written.
    :param delimiter: String, the separator of the entries of the line.
                        Default is a string comma ",".
    :param run: Integer, option to state if running the code
                is for the first time or not. Default is 0 (first time).

    :return: out_file: String, name of the output CSV file where
                        the information are written out.
    """
    if os.path.exists(out_file) and run == 0:
        os.remove(out_file)
    if not os.path.exists(out_file):
        action = "w"
    else:
        action = "a"

    with open(out_file, action) as domain_file:
        if action == "w":
            domain_file.write(f"contig{delimiter}contig_name{delimiter}"
                              f"start_gene{delimiter}stop_gene{delimiter}"
                              f"start_domain{delimiter}"
                              f"stop_domain{delimiter}domain\n")

        [domain_file.write(f"{hit}{delimiter}") for hit in parsed_line[:-1]]
        domain_file.write(f"{parsed_line[-1]}\n")
    return out_file


def main_parsing_ordered_genome_files(genome_filenames,
                                      delimiter=",",
                                      out_file="nvme_genome.csv",
                                      run=0):
    """Wrapper function for parsing genome data files to CSV.

    :param genome_filenames: List, containing string name of the genome
                                files names.
    :param delimiter: String, the separator of the entries of the line.
                        Default is a string comma ",".
    :param out_file: String, name of the output CSV file where
                        the information are written out.
    :param run: Integer, option to state if running the code
                is for the first time or not. Default is 0 (first time).

    :return: String, name of the output CSV file.
    """
    file_written, genome_filenames = None, genome_filenames.split(",")
    for files in genome_filenames:
        logging.info(f"----------WORKING WITH {files.upper()}-----------")
        genome_records = get_a_genome_record_at_a_time(files, delimiter)
        for record_sample in genome_records:
            genome_id = record_sample[0].split("/")[4]
            logging.info(f"Writing data for {genome_id}...")
            for line in record_sample:
                try:
                    line = parse_a_protein_record_line(line, delimiter)
                    file_written = write_csv_file_of_domains(line,
                                                             out_file,
                                                             delimiter,
                                                             run=run)
                    run += 1
                except Exception as e:
                    print(line)
                    logging.warning(f"{e} was encountered but handled.")
                    pass
            logging.info(f"Successfully write data for {genome_id}.")
        logging.info(f"Successfully done with {files.upper()}.")
    logging.info(f"Successfully Done Writing all the data to {out_file}.")
    return file_written


# ********** Second part: Arrange the CSV in operon or chromosome **********

def get_genome_sample_records(genomes_file, delimiter=","):
    """Get a genome sample in the raw data.

    :param genomes_file: String, name of data file containing genome records
    :param delimiter: String, the separator of the entries of the line.
                        Default is a string comma ",".

    :return: Object, a generator object (lazy iterator) of one genome
            sample line records at the time
    """
    genome_record, genome_id = [], None
    with open(genomes_file) as genome_lines:
        for line in genome_lines:
            if not line.strip():
                continue
            line = line.strip()
            if line.startswith("contig") or "contig" in line:
                continue
            try:
                line = (*line.split(delimiter),)
                line = (line[0], line[1], int(line[2]), int(line[3]),
                        int(line[4]), int(line[5]), line[6])
                assert len(line) == 7, f"Line of length 7 is expected. " \
                                       f"Found line of length {len(line)}"

                if genome_id is not None and genome_id != line[0] \
                        and genome_record:
                    contig_in_record = [c[0] for c in genome_record]
                    assert len(set(contig_in_record)) == 1, \
                        "More than one genome ID detected"
                    yield genome_record
                    genome_record, genome_id = [], None
                genome_id = line[0]
                genome_record.append(line)
            except AssertionError and IndexError:
                logging.warning(f"Something is wrong with genome {line[0]}."
                                f"This line is ignored.")
                pass
        if genome_record:
            yield genome_record


def find_contig_records_in_a_genome_sample(genome_record):
    """Find the various contig data in a genome records.

    :param genome_record: List or any iterable, list of the sting line
            of a genome records

    :return: Object, a generator object (lazy iterator) of one contig
            line records at the time
    """
    contig_record, contig_id = [], None
    for line in genome_record:
        try:
            if contig_id is not None and contig_id != line[1] and \
                    contig_record:
                contig_in_record = [c[1] for c in contig_record]
                assert len(set(contig_in_record)) == 1, \
                    "More than one contig detected"
                yield contig_record
                contig_record, contig_id = [], None
            contig_id = line[1]
            contig_record.append(line)
        except AssertionError and IndexError:
            logging.warning(f"Something is wrong with genome {line[0]}."
                            f"This line is ignored.")
            pass
    if contig_record:
        yield contig_record


def separate_strand_for_contig_record(contig_record):
    """Find and separate line of a contig per strand

    :param contig_record: List or iterable, containing tuple line
                            of a contig record.

    :return: Dictionary, with strand (forward, reverse) as key and
    list of tuples line corresponding to the strand in a contig record
    """
    strand_data = {}
    for line in contig_record:
        if not line[-1].startswith("c") and "forward" not in strand_data:
            strand_data["forward"] = [line]
        elif not line[-1].startswith("c") and "forward" in strand_data:
            strand_data["forward"].append(line)
        elif line[-1].startswith("c") and "reverse" not in strand_data:
            strand_data["reverse"] = [line]
        elif line[-1].startswith("c") and "reverse" in strand_data:
            strand_data["reverse"].append(line)
        else:
            logging.warning(f"Line {line} is neither on forward nor "
                            f"on reverse strand. This domain is ignored.")
    return strand_data


def find_gene_in_a_contig_record(gene_record):
    """Find protein in a gene record based on their start and end position.

    :param gene_record: List, containing tuples of domain line data.

    :return: list, containing list of tuples of line corresponding
            to the list of gene on a strand in a contig record.
    :raise ValueError if the tuple is not of length 7.
    """
    new_gene_record = deepcopy(gene_record)
    record_gene = [new_gene_record[0]]
    start_ref, stop_ref = new_gene_record[0][2], new_gene_record[0][3]
    for line in new_gene_record[1:]:
        try:
            contig, gene, start_gene, stop_gene, start_domain, \
            stop_domain, accession = line
            if (start_ref, stop_ref) != (start_gene, stop_gene) \
                    and record_gene:
                yield record_gene
                record_gene, start_ref, stop_ref = ([line],
                                                    start_gene, stop_gene)
            else:
                record_gene.append(line)
                start_ref, stop_ref = start_gene, stop_gene
        except IndexError:
            raise ValueError(f"Unexcepeted length encountered in {line}."
                             f"Length 7 is excepted {len(line)} found.")
    if record_gene:
        yield record_gene


def find_gene_on_each_strand(contig_record):
    """Find gene in a contig record based on their start and end position.

    :param contig_record: List or iterable, containing tuple line
                            of a contig record.

    :return: Dictionary, with strand (forward, reverse) as key and
            list of list containing tuples of line corresponding
            to the list of gene on a strand in a contig record
            ordered on their start and stop position.
    """
    strand_data = separate_strand_for_contig_record(contig_record)
    for strand in strand_data.keys():
        sorted_domain_list = []
        sorted_gene_per_contig = sort_by_start_and_stop(strand_data[strand])
        gene_data = find_gene_in_a_contig_record(sorted_gene_per_contig)
        for k in gene_data:
            sorted_domain_list.append(k)
        strand_data.update({strand: sorted_domain_list})
    return strand_data


def sort_by_start_and_stop(list_gene_domain, start_index=2,
                           stop_index=5):
    """Sort list of tuples line based on gene start and domain stop.

    :param list_gene_domain: list, containing tuples of domain line data.
    :param start_index: Integer, representing index of the start position
                        of gene in the tuple. Default is 2.
    :param stop_index: Integer, representing index of the stop position
                        of domain in the tuple. Default is 2.

    :return: list, containing sorted tuples based on the gene start
                    and domain stop.
    """
    return sorted(list_gene_domain,
                  key=lambda x: tuple(x[start_index:stop_index + 1]))


def find_operon_in_contig(dict_gene, dist=1000):
    """Retrieve domain architecture of genes in same operon in the genome.

    :param dict_gene: Dictionary, with strand as key and list of list
                    of tuple corresponding to line domain information
                        per genes and sorted based on start and stop.
    :param dist: Integer, desired distance between genes for been
                    in the same operon. Default is 1000.

    :return: Dictionary,  with operon number as key and list
                    of tuple domain in the specific operon.
    """
    operons_dict, i, operon = {}, 1, []
    # iterate over the strand
    for strand in dict_gene.keys():
        # check if there is any gene on the strand
        if dict_gene[strand]:
            try:
                # if there is one gene on the operon, teh gene is alone
                if len(dict_gene[strand]) == 1:
                    operons_dict[f"operon {i}"] = dict_gene[strand][0]
                    i += 1
                else:
                    init_gene = dict_gene[strand][0]  # First gene in operon
                    operon = init_gene
                    list_start_stop = [(x[2], x[3]) for x in init_gene]
                    # check if all tuple domain in gene have same coordinate
                    assert len(set(list_start_stop)) == 1
                    init_start, init_end = init_gene[0][2], init_gene[0][3]
                    if init_start < init_end:
                        start_gene = set(list(range(init_start,
                                                    init_end + 1 + dist)))
                    else:
                        start_gene = set(list(range(init_start,
                                                    init_end + 1 + dist,
                                                    -1)))
                    for gene in dict_gene[strand][1:]:
                        list_start_stop = [(x[2], x[3]) for x in gene]
                        assert len(set(list_start_stop)) == 1
                        cur_start, cur_end = gene[0][2], gene[0][3]
                        if cur_start < cur_end:
                            cur_gene = set(list(range(cur_start,
                                                      cur_end + 1 + dist)))
                        else:
                            cur_gene = set(list(range(cur_start,
                                                      cur_end + 1 + dist,
                                                      -1)))

                        # check if there if an overlap between genes
                        if start_gene.intersection(cur_gene):
                            operon = operon + gene
                            start_gene = cur_gene
                        else:
                            operons_dict[f"operon {i}"] = operon
                            operon = deepcopy(gene)
                            start_gene = cur_gene
                            i += 1
                    if operon:
                        operons_dict[f"operon {i}"] = operon
                        i += 1
            except AssertionError:
                logging.warning(f"Error in some genes....")
                pass
    return operons_dict


def combined_by_domain_in_architecture(sorted_domain_list,
                                       join_with=" "):
    """Combine domain into a string.

    :param sorted_domain_list: List, list of list of tuple
                                containing  domain data
    :param join_with: String, the separator of the prtein domain
                        architectures. Default is a string comma ","

    :return: Dict, dictionary of genome ID as key and string domain
                    architecture as value
    """
    domain_data = {}
    if sorted_domain_list:
        contig_id = set([x[0] for protein in
                         sorted_domain_list for x in protein])
        assert (len(contig_id) == 1), \
            "Only one genome should be used but more than one detected"
        contig_id = list(contig_id)[0]
        for gene_list in sorted_domain_list:
            domain_order_list = [x[-1] for x in gene_list]
            domain_order_string = join_with.join(domain_order_list)
            if contig_id not in domain_data:
                domain_data[contig_id] = [domain_order_string]
            domain_data[contig_id].append(domain_order_string)
    return domain_data


def make_domain_architecture_per_operon(domain_in_operon,
                                        join_with=" "):
    """Combine domain in the same operon into a string.

    :param domain_in_operon: List, sorted list of the domain tuple
                                in the same operon
    :param join_with: String, the separator of the prtein domain
                        architectures. Default is a string comma ","

    :return: Dict, dictionary of genome ID as key and string domain
                    architecture of the operon as value
    """
    domain_data = {}
    if domain_in_operon:
        contig_id = set([x[0] for x in domain_in_operon])
        assert (len(contig_id) == 1), \
            "Only one genome should be used but more than one detected"
        contig_id = list(contig_id)[0]
        domain_order_list = [x[-1][-7:] for x in domain_in_operon]
        domain_order_string = join_with.join(domain_order_list)
        if contig_id not in domain_data:
            domain_data[contig_id] = [domain_order_string]
        domain_data[contig_id].append(domain_order_string)
    return domain_data


def write_domains_architecture_in_genome_to_file(record_line,
                                                 out_file,
                                                 separator,
                                                 run=0):
    """Write out domain architecture in a genome to CSV file.

    :param record_line: Dictionary, with  genome ID as key and list
                        of sting domain architecture as values.
    :param out_file: String, name of the output CSV file where
                        the information are to be written out.
    :param run: Integer, option to state if running the code
                is for the first time or not. Default is 0 (first time).
    :param separator: String, the separator of domain architecture.
                        Default is a string comma ". ".

    :return: Integer, indicating the number of genome that were not
                written due to suspisious data format or without data
    """
    not_written_record = 0
    if record_line:
        assert len(record_line.keys()) == 1, \
            "Only sample required, more than one found"
        for key in record_line:
            to_be_written = record_line[key]
            # to_be_written.insert(0, key)
            if os.path.exists(out_file) and run == 0:
                os.remove(out_file)
            if not os.path.exists(out_file):
                action = "w"
            else:
                action = "a"
            with open(out_file, action) as domain_file:
                if action == "w":
                    domain_file.write("contig, domain\n")

                domain_file.write(f"{key},")

                [domain_file.write(f"{hit}{separator}") for
                 hit in to_be_written[:-1]]

                domain_file.write(f"{to_be_written[-1]}\n")
    else:
        not_written_record += 1
    return not_written_record


def main_write_data_per_genome(domain_file,
                               out_file="genome_domain_architecture.csv",
                               delimiter=",",
                               separator=". ",
                               join_with=" ",
                               run=0):
    """Wrapper for writting domain architecture per genome to CSV file.

    :param domain_file: String, name of data file containing genome records
    :param out_file: String, name of the output CSV file where the
                        information are to be written out.
                        Default is "genome_domain_architecture.csv"
    :param delimiter:  String, the separator of the entries of the line.
                        Default is a string comma ",".
    :param separator: String, the separator of domain architecture.
                        Default is the string  ". ".
    :param join_with:  String, the separator to be used to join domain
                    together in architecture. Default is a string space " ".
    :param run: Integer, option to state if running the code
                is for the first time or not. Default is 0 (first time).

    :return: No return... or return None
    """
    unwritten_genome = 0
    # find genomes in data
    sample_generator = get_genome_sample_records(domain_file, delimiter)
    # iterate over the genomes
    for sample in sample_generator:
        id_contig = sample[0][0]
        logging.info(f"-----WORKING WITH SAMPLE {id_contig}. "
                     f"This is the {run + 1}th genome record-----")
        # find contig in genome
        contig_in_genome = find_contig_records_in_a_genome_sample(sample)
        sorted_domain_list = []  # to store sorted gene per contig in genome
        # iterate over each contig
        for contig_rec in contig_in_genome:
            contig_data = []  # to store gene data per contig data
            # find and seperate genes on the two strand in a genome
            gene_per_strand = find_gene_on_each_strand(contig_rec)
            # iterate over each strand
            for strand in gene_per_strand.keys():
                # append the data from reverse to the one of forward
                contig_data = contig_data + gene_per_strand[strand]
            # append sorted gene data of the contig data to genome data list
            sorted_domain_list = sorted_domain_list + contig_data
        # combine data in the genome list
        combine_dom = combined_by_domain_in_architecture(sorted_domain_list,
                                                         join_with)
        # write genome data to file
        t = write_domains_architecture_in_genome_to_file(combine_dom,
                                                         out_file,
                                                         separator,
                                                         run)
        run += 1
        unwritten_genome += t
        logging.info(f"Successfully write data for {id_contig} with "
                     f"{unwritten_genome} suspicious genome, not written.")
    logging.info(f"Successfully Done.")


def main_write_data_per_contig_in_genome(domain_file,
                                         out_file="contig_domain_\
                                         architecture.csv",
                                         delimiter=",",
                                         separator=". ",
                                         join_with=" ",
                                         run=0):
    """Wrapper for writting contig domain architecture in genome to file.

    :param domain_file: String, name of data file containing genome records
    :param out_file: String, name of the output CSV file where the
                        information are to be written out.
                        Default is "contig_domain_architecture.csv"
    :param delimiter:  String, the separator of the entries of the line.
                        Default is a string comma ",".
    :param separator: String, the separator of domain architecture.
                        Default is the string  ". ".
    :param join_with:  String, the separator to be used to join domain
                    together in architecture. Default is a string space " ".
    :param run: Integer, option to state if running the code
                is for the first time or not. Default is 0 (first time).

    :return: No return... or return None
    """
    unwritten_genome = 0
    # find genomes in data
    sample_generator = get_genome_sample_records(domain_file, delimiter)
    # iterate over the genomes
    for sample in sample_generator:
        id_contig = sample[0][0]
        logging.info(f"-----WORKING WITH SAMPLE {id_contig}. "
                     f"This is the {run + 1}th genome record-----")
        # find contig in genome
        contig_in_genome = find_contig_records_in_a_genome_sample(sample)
        # iterate over each contig
        for contig_rec in contig_in_genome:
            contig_data = []  # to store gene data per contig data
            # find and seperate genes on the two strand in a genome
            gene_per_strand = find_gene_on_each_strand(contig_rec)
            # iterate over each strand
            for strand in gene_per_strand.keys():
                # append the data from reverse to the one of forward
                contig_data = contig_data + gene_per_strand[strand]
            # combine data in the genome list
            combine_dom = combined_by_domain_in_architecture(contig_data,
                                                             join_with)
            # write genome data to file
            t = write_domains_architecture_in_genome_to_file(combine_dom,
                                                             out_file,
                                                             separator,
                                                             run)
            run += 1
            unwritten_genome += t
        logging.info(f"Successfully write data for {id_contig} with "
                     f"{unwritten_genome} suspicious genome, not written.")
    logging.info(f"Successfully Done.")


def main_write_data_per_operon_in_genome(domain_file,
                                         dist=1000,
                                         out_file="operon_domain_\
                                         architecture.csv",
                                         delimiter=",",
                                         separator=". ",
                                         join_with=" ",
                                         run=0):
    """Wrapper for writting operon domain architecture in genome to file.

    :param domain_file: String, name of data file containing genome records
    :param dist: Integer, desired distance between genes for been
                    in the same operon. Default is 1000.
    :param out_file: String, name of the output CSV file where the
                        information are to be written out. Default is
                        Default is "operon_domain_architecture.csv"
    :param delimiter:  String, the separator of the entries of the line.
                        Default is a string comma ",".
    :param separator: String, the separator of domain architecture.
                        Default is the string  ". ".
    :param join_with:  String, the separator to be used to join domain
                    together in architecture. Default is a string space " ".
    :param run: Integer, option to state if running the code
                is for the first time or not. Default is 0 (first time).

    :return: No return... or return None
    """
    unwritten_genome, i = 0, 0
    # find genomes in data
    sample_generator = get_genome_sample_records(domain_file, delimiter)
    # iterate over the genomes
    for sample in sample_generator:
        id_contig = sample[0][0]
        logging.info(f"-----WORKING WITH SAMPLE {id_contig}. "
                     f"This is the {i + 1}th genome record-----")
        # find contig in genome
        contig_in_genome = find_contig_records_in_a_genome_sample(sample)
        # iterate over each contig
        for contig_rec in contig_in_genome:
            # find and seperate genes on the two strand in a genome
            gene_per_strand = find_gene_on_each_strand(contig_rec)
            operons = find_operon_in_contig(gene_per_strand, dist)
            # iterate over each operon
            for operon in operons.keys():
                # combine data in operon domains
                # print(operons[operon])
                comb_dom = make_domain_architecture_per_operon(
                    operons[operon], join_with)
                # write operon data to file
                t = write_domains_architecture_in_genome_to_file(comb_dom,
                                                                 out_file,
                                                                 separator,
                                                                 run)
                unwritten_genome += t
                run += 1
        i += 1
        logging.info(f"Successfully write data for {id_contig} with "
                     f"{unwritten_genome} suspicious operons, not written.")
    logging.info(f"Successfully Done.")


def parse_args():
    """Get argumet from command line and parse into main function.

    :return: the parsed command line arguments.
    """
    parser = argparse.ArgumentParser(description="This Script parse ordered\
     domain in genome, and make appropriate domain architecture in genome,\
      contig or operon depending of your interest and specified paraeters.")

    parser.add_argument("-u", "--use", type=str, default="operon",
                        choices=["contig", "operon", "genome"],
                        help="Level at which domain architecture\
                         should be put together.", required=True)
    parser.add_argument("-g", "--raw_genome",
                        help="List of full path or file name of the raw \
                        genome file", required=True)
    parser.add_argument("-d", "--delimiter", type=str, default=",",
                        help="Delimiter of entries on each line of \
                        genome file")
    parser.add_argument("-c", "--coordinate_domain", type=str,
                        default="nvme_genome.csv",
                        help="Full path or file name of the output of\
                        domain coordinates file")
    parser.add_argument("-dist", "--distance", type=int, default=100,
                        help="Desired distance between  operons")
    parser.add_argument("-o", "--output", type=str,
                        default="operon_domain_architecture.csv",
                        help="Full path of file name of the final \
                        output file")
    parser.add_argument("-s", "--separator", type=str, default=". ",
                        help="separator between two domain architecture.")
    parser.add_argument("-j", "--join_with", type=str, default=". ",
                        help="separator between 2 domain on the same gene.")
    args = parser.parse_args()
    return args


def main():
    """This main function runs the script based on the specified arguments.

    :return: None
    """
    args = parse_args()
    if os.path.exists(args.coordinate_domain):
        logging.info(f"** coordinate file exists. skip making new file.**")
        domain_coord = args.coordinate_domain
    else:
        logging.info(f"** coordinate file does not exist. Making....**")
        domain_coord = main_parsing_ordered_genome_files(args.raw_genome,
                                                         args.delimiter,
                                                         args.coordinate_domain)
    if args.use == "operon":
        main_write_data_per_operon_in_genome(domain_coord,
                                             args.distance,
                                             args.output,
                                             args.delimiter,
                                             args.separator,
                                             args.join_with)
    elif args.use == "contig":
        main_write_data_per_contig_in_genome(domain_coord,
                                             args.output,
                                             args.delimiter,
                                             args.seperator,
                                             args.join_with)
    elif args.use == "genome":
        main_write_data_per_genome(domain_coord,
                                   args.output,
                                   args.delimiter,
                                   args.seperator,
                                   args.join_with)
    else:
        raise ValueError("Please specify a valid use. "
                         "It is either 'contig', 'genome', or 'operon'.")


if __name__ == '__main__':
    main()
